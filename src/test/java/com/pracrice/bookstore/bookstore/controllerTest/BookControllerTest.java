package com.pracrice.bookstore.bookstore.controllerTest;

import java.net.URI;
import java.net.URISyntaxException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.practice.bookstore.bookstore.BookStoreApplication;
import com.practice.bookstore.bookstore.entity.Author;
import com.practice.bookstore.bookstore.entity.Book;
import com.practice.bookstore.bookstore.repository.BookRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT,classes = BookStoreApplication.class)
public class BookControllerTest {
	@Autowired
    private TestRestTemplate restTemplate;
	
	@Autowired
	private BookRepository repository;
	
	@LocalServerPort
    private int randomServerPort;
	
	@After
	public void resetDb() {
		repository.deleteAll();
	    repository.flush();
	}
	
	public Book createTestBook(String name) {
		Book book = new Book(name,12,"by 1", new Date(System.currentTimeMillis()),"desc1", 122, null, null);
		return repository.saveAndFlush(book);
	}
	
    @Test
    public void testAddBookSuccess() throws URISyntaxException 
    {
        String baseUrl = "http://localhost:" + randomServerPort + "/api/books";
        URI uri = new URI(baseUrl);
         
        HttpHeaders headers = new HttpHeaders();
        headers.set("X-COM-PERSIST", "true");      
        headers.set("Content-Type", "application/json");    
        
        Book book = new Book("Book1",12,"by AddBookSuccess", new Date(System.currentTimeMillis()),"desc1", 122, null, null);
        HttpEntity<Book> request = new HttpEntity<>(book, headers);
         
        ResponseEntity<Book> result = this.restTemplate.postForEntity(uri, request, Book.class);
         
        //Verify request succeed
        Assert.assertEquals(result.getStatusCodeValue(), 201);
        Assert.assertNotNull(result.getBody().getId());
        Assert.assertEquals(result.getBody().getName(), "Book1");
        Assert.assertEquals(result.getBody().getDescription(), "desc1");
        Assert.assertEquals(result.getBody().getCreated_by(), "by AddBookSuccess");
        Assert.assertEquals(result.getBody().getSku(), 12);
        Assert.assertNull(result.getBody().getAuthor());
        Assert.assertNull(result.getBody().getCathegory());
    }
    
    @Test
    public void testGetBookSuccess() {
    	String baseUrl = "http://localhost:" + randomServerPort + "/api/books/{id}";
        
        long id = createTestBook("Book1").getId();
        Book result = restTemplate.getForObject(baseUrl, Book.class,id);
        
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getId());
        Assert.assertEquals(result.getName(), "Book1");
        Assert.assertEquals(result.getDescription(), "desc1");
        Assert.assertEquals(result.getCreated_by(), "by 1");
        Assert.assertEquals(result.getSku(), 12);
        Assert.assertNull(result.getAuthor());
        Assert.assertNull(result.getCathegory());
    }
    
    @Test
    public void testPutBookSuccess() {
    	String baseUrl = "http://localhost:" + randomServerPort + "/api/books/{id}";
    	
    	long id = createTestBook("Book1").getId();
        Book book = new Book("Book2",13,"by PutBookSuccess", new Date(System.currentTimeMillis()),"desc2", 123, null, null);
    	HttpEntity<Book> entity = new HttpEntity<Book>(book);
    	ResponseEntity<Book> response = restTemplate.exchange(baseUrl, HttpMethod.PUT, entity, Book.class, id);
    	
    	Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);
    	Assert.assertNotNull(response.getBody().getId());
    	Assert.assertEquals(response.getBody().getName(), "Book2");
        Assert.assertEquals(response.getBody().getSku(), 13);
        Assert.assertEquals(response.getBody().getCreated_by(), "by PutBookSuccess");
        Assert.assertEquals(response.getBody().getDescription(), "desc2");
        Assert.assertNull(response.getBody().getAuthor());
        Assert.assertNull(response.getBody().getCathegory());
    }
    
    @Test
    public void testGetAllBooks() {
    	String baseUrl = "http://localhost:" + randomServerPort + "/api/books";
    	
    	createTestBook("Book1");
    	createTestBook("Book2");
    	ResponseEntity<List<Book>> response = restTemplate.exchange(baseUrl, HttpMethod.GET, null,
    	    new ParameterizedTypeReference<List<Book>>() {
    	    });
    	  List<Book> books = response.getBody();
    	  Assert.assertEquals(books.size(), 2);
    	  Assert.assertEquals(books.get(0).getName(), "Book1");
    	  Assert.assertEquals(books.get(1).getName(), "Book2");
    }
    
    @Test
    public void testDeleteBookSuccess() {
    	String baseUrl = "http://localhost:" + randomServerPort + "/api/books/{id}";
    	
    	long id = createTestBook("Book1").getId();
    	ResponseEntity<Book> response = restTemplate.exchange(baseUrl, HttpMethod.DELETE, null, Book.class, id);
    	
    	Assert.assertEquals(response.getStatusCode(), HttpStatus.NO_CONTENT);
    }
    
    @Test
    public void testDeleteAllAuthorsSuccess() {
    	String baseUrl = "http://localhost:" + randomServerPort + "/api/books";
    	
    	createTestBook("Book1");
    	createTestBook("Book2");
    	ResponseEntity<Book> response = restTemplate.exchange(baseUrl, HttpMethod.DELETE, null, Book.class);
    	
    	Assert.assertEquals(response.getStatusCode(), HttpStatus.NO_CONTENT);
    }
 
}