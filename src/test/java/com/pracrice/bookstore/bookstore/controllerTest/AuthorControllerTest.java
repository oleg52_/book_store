package com.pracrice.bookstore.bookstore.controllerTest;

import java.net.URI;
import java.net.URISyntaxException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.practice.bookstore.bookstore.BookStoreApplication;
import com.practice.bookstore.bookstore.entity.Author;
import com.practice.bookstore.bookstore.entity.Book;
import com.practice.bookstore.bookstore.repository.AuthorRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT,classes = BookStoreApplication.class)
public class AuthorControllerTest {
	@Autowired
    private TestRestTemplate restTemplate;
	
	@Autowired
	private AuthorRepository repository;
	
	@LocalServerPort
    private int randomServerPort;
	
	@After
	public void resetDb() {
		repository.deleteAll();
	    repository.flush();
	}
	
	public Author createTestAuthor(String name) {
		Author author = new Author(name,"auth1",new Date(System.currentTimeMillis()),"male", null);
        List<Book> books = new ArrayList<>();
        books.add(new Book("book1",12,"by 1",new Date(System.currentTimeMillis()),"desc1", 122, author, null));
        books.add(new Book("book2",13,"by 2",new Date(System.currentTimeMillis()),"desc2", 124, author, null));
        author.setWritten_books(books);
		return repository.saveAndFlush(author);
	}
	
    @Test
    public void testAddAuthorSuccess() throws URISyntaxException 
    {
        String baseUrl = "http://localhost:" + randomServerPort + "/api/authors";
        URI uri = new URI(baseUrl);
         
        HttpHeaders headers = new HttpHeaders();
        headers.set("X-COM-PERSIST", "true");      
        headers.set("Content-Type", "application/json");    
        
        Author author = new Author("Bill","Bill1",new Date(System.currentTimeMillis()),"male", null);
        List<Book> books = new ArrayList<>();
        books.add(new Book("bookBill1",12,"by 1",new Date(System.currentTimeMillis()),"desc1", 122, author, null));
        books.add(new Book("bookBill2",13,"by 2",new Date(System.currentTimeMillis()),"desc2", 123, author, null));
        author.setWritten_books(books);
        HttpEntity<Author> request = new HttpEntity<>(author, headers);
         
        ResponseEntity<Author> result = this.restTemplate.postForEntity(uri, request, Author.class);
         
        //Verify request succeed
        Assert.assertEquals(result.getStatusCodeValue(), 201);
        Assert.assertNotNull(result.getBody().getId());
        Assert.assertEquals(result.getBody().getFirst_name(), "Bill");
        Assert.assertEquals(result.getBody().getLast_name(), "Bill1");
        Assert.assertEquals(result.getBody().getGender(), "male");
        Assert.assertNotNull(result.getBody().getWritten_books());
        Assert.assertEquals(result.getBody().getWritten_books().size(), 2);
    }
    
    @Test
    public void testGetAuthorSuccess() {
    	String baseUrl = "http://localhost:" + randomServerPort + "/api/authors/{id}";
        
        long id = createTestAuthor("Joe").getId();
        Author result = restTemplate.getForObject(baseUrl, Author.class,id);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getId());
        Assert.assertEquals(result.getFirst_name(), "Joe");
        Assert.assertEquals(result.getLast_name(), "auth1");
        Assert.assertEquals(result.getGender(), "male");
        Assert.assertNotNull(result.getWritten_books());
        Assert.assertEquals(result.getWritten_books().size(), 2);
    }
    
    @Test
    public void testPutAuthorSuccess() {
    	String baseUrl = "http://localhost:" + randomServerPort + "/api/authors/{id}";
    	
    	long id = createTestAuthor("John").getId();
    	Author author = new Author("John1","John2",new Date(System.currentTimeMillis()),"male", null);
    	HttpEntity<Author> entity = new HttpEntity<Author>(author);
    	ResponseEntity<Author> response = restTemplate.exchange(baseUrl, HttpMethod.PUT, entity, Author.class, id);
    	
    	Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);
    	Assert.assertNotNull(response.getBody().getId());
    	Assert.assertEquals(response.getBody().getFirst_name(), "John1");
        Assert.assertEquals(response.getBody().getLast_name(), "John2");
        Assert.assertEquals(response.getBody().getGender(), "male");
        Assert.assertNotNull(response.getBody().getWritten_books());
        Assert.assertEquals(response.getBody().getWritten_books().size(), 2);
    }
    
    @Test
    public void testGetAllAuthors() {
    	String baseUrl = "http://localhost:" + randomServerPort + "/api/authors";
    	
    	createTestAuthor("Joe");
    	createTestAuthor("Jane");
    	ResponseEntity<List<Author>> response = restTemplate.exchange(baseUrl, HttpMethod.GET, null,
    	    new ParameterizedTypeReference<List<Author>>() {
    	    });
    	  List<Author> authors = response.getBody();
    	  Assert.assertEquals(authors.size(), 2);
    	  Assert.assertEquals(authors.get(0).getFirst_name(), "Joe");
    	  Assert.assertEquals(authors.get(1).getFirst_name(), "Jane");
    }
    
    @Test
    public void testDeleteAuthorSuccess() {
    	String baseUrl = "http://localhost:" + randomServerPort + "/api/authors/{id}";
    	
    	long id = createTestAuthor("John").getId();
    	ResponseEntity<Author> response = restTemplate.exchange(baseUrl, HttpMethod.DELETE, null, Author.class, id);
    	
    	Assert.assertEquals(response.getStatusCode(), HttpStatus.NO_CONTENT);
    }
    
    @Test
    public void testDeleteAllAuthorsSuccess() {
    	String baseUrl = "http://localhost:" + randomServerPort + "/api/authors";
    	
    	createTestAuthor("John");
    	createTestAuthor("Jade");
    	ResponseEntity<Author> response = restTemplate.exchange(baseUrl, HttpMethod.DELETE, null, Author.class);
    	
    	Assert.assertEquals(response.getStatusCode(), HttpStatus.NO_CONTENT);
    }
 
}
