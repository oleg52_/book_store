package com.pracrice.bookstore.bookstore.controllerTest;

import java.net.URI;
import java.net.URISyntaxException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.practice.bookstore.bookstore.BookStoreApplication;
import com.practice.bookstore.bookstore.entity.Cathegory;
import com.practice.bookstore.bookstore.entity.Book;
import com.practice.bookstore.bookstore.entity.Cathegory;
import com.practice.bookstore.bookstore.repository.CathegoryRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT,classes = BookStoreApplication.class)
public class CathegoryControllerTest {
	@Autowired
    private TestRestTemplate restTemplate;
	
	@Autowired
	private CathegoryRepository repository;
	
	@LocalServerPort
    private int randomServerPort;
	
	@After
	public void resetDb() {
		repository.deleteAll();
	    repository.flush();
	}
	
	public Cathegory createTestCathegory(String name) {
		Cathegory cathegory = new Cathegory(name,"by 1",new Date(System.currentTimeMillis()), null);
        List<Book> books = new ArrayList<>();
        books.add(new Book("book1",12,"by 1",new Date(System.currentTimeMillis()),"desc1", 122, null, cathegory));
        books.add(new Book("book2",13,"by 2",new Date(System.currentTimeMillis()),"desc2", 124, null, cathegory));
        cathegory.setContained_books(books);
		return repository.saveAndFlush(cathegory);
	}
	
    @Test
    public void testAddCathegorySuccess() throws URISyntaxException 
    {
        String baseUrl = "http://localhost:" + randomServerPort + "/api/cathegories";
        URI uri = new URI(baseUrl);
         
        HttpHeaders headers = new HttpHeaders();
        headers.set("X-COM-PERSIST", "true");      
        headers.set("Content-Type", "application/json");    
        
        Cathegory cathegory = new Cathegory("Cath1","by addCathSucc",new Date(System.currentTimeMillis()), null);
        List<Book> books = new ArrayList<>();
        books.add(new Book("book1",12,"by 1",new Date(System.currentTimeMillis()),"desc1", 122, null, cathegory));
        books.add(new Book("book2",13,"by 2",new Date(System.currentTimeMillis()),"desc2", 124, null, cathegory));
        cathegory.setContained_books(books);
        HttpEntity<Cathegory> request = new HttpEntity<>(cathegory, headers);
         
        ResponseEntity<Cathegory> result = this.restTemplate.postForEntity(uri, request, Cathegory.class);
         
        //Verify request succeed
        Assert.assertEquals(result.getStatusCodeValue(), 201);
        Assert.assertNotNull(result.getBody().getId());
        Assert.assertEquals(result.getBody().getName(), "Cath1");
        Assert.assertEquals(result.getBody().getCreated_by(), "by addCathSucc");
        Assert.assertNotNull(result.getBody().getContained_books());
        Assert.assertEquals(result.getBody().getContained_books().size(), 2);
    }
    
    @Test
    public void testGetCathegorySuccess() {
    	String baseUrl = "http://localhost:" + randomServerPort + "/api/cathegories/{id}";
        
        long id = createTestCathegory("Cath2").getId();
        Cathegory result = restTemplate.getForObject(baseUrl, Cathegory.class, id);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getId());
        Assert.assertEquals(result.getName(), "Cath2");
        Assert.assertEquals(result.getCreated_by(), "by 1");
        Assert.assertNotNull(result.getContained_books());
        Assert.assertEquals(result.getContained_books().size(), 2);
    }
    
    @Test
    public void testPutCathegorySuccess() {
    	String baseUrl = "http://localhost:" + randomServerPort + "/api/cathegories/{id}";
    	
    	long id = createTestCathegory("Cath1").getId();
        Cathegory cathegory = new Cathegory("Cath2","by putCathSucc",new Date(System.currentTimeMillis()), null);
    	HttpEntity<Cathegory> entity = new HttpEntity<Cathegory>(cathegory);
    	ResponseEntity<Cathegory> response = restTemplate.exchange(baseUrl, HttpMethod.PUT, entity, Cathegory.class, id);
    	
    	Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);
    	Assert.assertNotNull(response.getBody().getId());
    	Assert.assertEquals(response.getBody().getName(), "Cath2");
        Assert.assertEquals(response.getBody().getCreated_by(), "by putCathSucc");
        Assert.assertNotNull(response.getBody().getContained_books());
        Assert.assertEquals(response.getBody().getContained_books().size(), 2);
    }
    
    @Test
    public void testGetAllCathegorys() {
    	String baseUrl = "http://localhost:" + randomServerPort + "/api/cathegories";
    	
    	createTestCathegory("Cath1");
    	createTestCathegory("Cath2");
    	ResponseEntity<List<Cathegory>> response = restTemplate.exchange(baseUrl, HttpMethod.GET, null,
    	    new ParameterizedTypeReference<List<Cathegory>>() {
    	    });
    	  List<Cathegory> cathegorys = response.getBody();
    	  Assert.assertEquals(cathegorys.size(), 2);
    	  Assert.assertEquals(cathegorys.get(0).getName(), "Cath1");
    	  Assert.assertEquals(cathegorys.get(1).getName(), "Cath2");
    }
    
    @Test
    public void testDeleteCathegorySuccess() {
    	String baseUrl = "http://localhost:" + randomServerPort + "/api/cathegories/{id}";
    	
    	long id = createTestCathegory("Cath1").getId();
    	ResponseEntity<Cathegory> response = restTemplate.exchange(baseUrl, HttpMethod.DELETE, null, Cathegory.class, id);
    	
    	Assert.assertEquals(response.getStatusCode(), HttpStatus.NO_CONTENT);
    }
    
    @Test
    public void testDeleteAllCathegorysSuccess() {
    	String baseUrl = "http://localhost:" + randomServerPort + "/api/cathegories";
    	
    	createTestCathegory("Cath1");
    	createTestCathegory("Cath2");
    	ResponseEntity<Cathegory> response = restTemplate.exchange(baseUrl, HttpMethod.DELETE, null, Cathegory.class);
    	
    	Assert.assertEquals(response.getStatusCode(), HttpStatus.NO_CONTENT);
    }
 
}
