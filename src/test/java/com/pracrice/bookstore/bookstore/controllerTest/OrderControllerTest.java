package com.pracrice.bookstore.bookstore.controllerTest;

import java.net.URI;
import java.net.URISyntaxException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.practice.bookstore.bookstore.BookStoreApplication;
import com.practice.bookstore.bookstore.entity.Order;
import com.practice.bookstore.bookstore.entity.Order_item;
import com.practice.bookstore.bookstore.entity.Book;
import com.practice.bookstore.bookstore.repository.OrderRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT,classes = BookStoreApplication.class)
public class OrderControllerTest {
	@Autowired
    private TestRestTemplate restTemplate;
	
	@Autowired
	private OrderRepository repository;
	
	@LocalServerPort
    private int randomServerPort;
	
	@After
	public void resetDb() {
		repository.deleteAll();
	    repository.flush();
	}
	
	public Order createTestOrder(String name) {
		Order order = new Order(name,"by 1",new Date(System.currentTimeMillis()),444, null);
        List<Order_item> order_items = new ArrayList<>();
        Book book = new Book("Book1",12,"by 1",new Date(System.currentTimeMillis()),"desc1", 122, null, null);
        Book book1 = new Book("Book2",12,"by 2",new Date(System.currentTimeMillis()),"desc2", 123, null, null);
        order_items.add(new Order_item(book, 12, 144, null));
        order_items.add(new Order_item(book1, 13, 145, null));
        order.setOrder_items(order_items);
		return repository.saveAndFlush(order);
	}
	
    @Test
    public void testAddOrderSuccess() throws URISyntaxException 
    {
        String baseUrl = "http://localhost:" + randomServerPort + "/api/orders";
        URI uri = new URI(baseUrl);
         
        HttpHeaders headers = new HttpHeaders();
        headers.set("X-COM-PERSIST", "true");      
        headers.set("Content-Type", "application/json");    
        
        Order order = new Order("Order1","by 1",new Date(System.currentTimeMillis()),444, null);
        List<Order_item> order_items = new ArrayList<>();
        Book book = new Book("Book1",12,"by 1",new Date(System.currentTimeMillis()),"desc1", 122, null, null);
        Book book1 = new Book("Book2",12,"by 2",new Date(System.currentTimeMillis()),"desc2", 123, null, null);
        order_items.add(new Order_item(book, 12, 144, null));
        order_items.add(new Order_item(book1, 13, 145, null));
        order.setOrder_items(order_items);
        HttpEntity<Order> request = new HttpEntity<>(order, headers);
         
        ResponseEntity<Order> result = this.restTemplate.postForEntity(uri, request, Order.class);
         
        //Verify request succeed
        Assert.assertEquals(result.getStatusCodeValue(), 201);
        Assert.assertNotNull(result.getBody().getId());
        Assert.assertEquals(result.getBody().getName(), "Order1");
        Assert.assertEquals(result.getBody().getCreated_by(), "by 1");
        Assert.assertEquals(result.getBody().getTotal_price(), 444, 0);
        Assert.assertNotNull(result.getBody().getOrder_items());
        Assert.assertEquals(result.getBody().getOrder_items().size(), 2);
        Assert.assertEquals(result.getBody().getOrder_items().get(0).getBook().getName(), "Book1");
        Assert.assertEquals(result.getBody().getOrder_items().get(1).getBook().getName(), "Book2");
    }
    
    @Test
    public void testGetOrderSuccess() {
    	String baseUrl = "http://localhost:" + randomServerPort + "/api/orders/{id}";
        
        long id = createTestOrder("Order1").getId();
        Order result = restTemplate.getForObject(baseUrl, Order.class,id);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getId());
        Assert.assertEquals(result.getName(), "Order1");
        Assert.assertEquals(result.getCreated_by(), "by 1");
        Assert.assertEquals(result.getTotal_price(), 444, 0);
        Assert.assertNotNull(result.getOrder_items());
        Assert.assertEquals(result.getOrder_items().size(), 2);
        Assert.assertEquals(result.getOrder_items().get(0).getBook().getName(), "Book1");
        Assert.assertEquals(result.getOrder_items().get(1).getBook().getName(), "Book2");
    }
    
    @Test
    public void testPutOrderSuccess() {
    	String baseUrl = "http://localhost:" + randomServerPort + "/api/orders/{id}";
    	
    	long id = createTestOrder("Order1").getId();
        Order order = new Order("Order2","by 2",new Date(System.currentTimeMillis()),445, null);
    	HttpEntity<Order> entity = new HttpEntity<Order>(order);
    	ResponseEntity<Order> response = restTemplate.exchange(baseUrl, HttpMethod.PUT, entity, Order.class, id);
    	
    	Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);
    	Assert.assertNotNull(response.getBody().getId());
    	Assert.assertEquals(response.getBody().getName(), "Order2");
        Assert.assertEquals(response.getBody().getCreated_by(), "by 2");
        Assert.assertEquals(response.getBody().getTotal_price(), 445, 0);
        Assert.assertNotNull(response.getBody().getOrder_items());
        Assert.assertEquals(response.getBody().getOrder_items().size(), 2);
        Assert.assertEquals(response.getBody().getOrder_items().get(0).getBook().getName(), "Book1");
        Assert.assertEquals(response.getBody().getOrder_items().get(1).getBook().getName(), "Book2");
    }
    
    @Test
    public void testGetAllOrders() {
    	String baseUrl = "http://localhost:" + randomServerPort + "/api/orders";
    	
    	createTestOrder("Order1");
    	createTestOrder("Order2");
    	ResponseEntity<List<Order>> response = restTemplate.exchange(baseUrl, HttpMethod.GET, null,
    	    new ParameterizedTypeReference<List<Order>>() {
    	    });
    	  List<Order> orders = response.getBody();
    	  Assert.assertEquals(orders.size(), 2);
    	  Assert.assertEquals(orders.get(0).getName(), "Order1");
    	  Assert.assertEquals(orders.get(1).getName(), "Order2");
    }
    
    @Test
    public void testDeleteOrderSuccess() {
    	String baseUrl = "http://localhost:" + randomServerPort + "/api/orders/{id}";
    	
    	long id = createTestOrder("Order1").getId();
    	ResponseEntity<Order> response = restTemplate.exchange(baseUrl, HttpMethod.DELETE, null, Order.class, id);
    	
    	Assert.assertEquals(response.getStatusCode(), HttpStatus.NO_CONTENT);
    }
    
    @Test
    public void testDeleteAllOrdersSuccess() {
    	String baseUrl = "http://localhost:" + randomServerPort + "/api/orders";
    	
    	createTestOrder("Order1");
    	createTestOrder("Order2");
    	ResponseEntity<Order> response = restTemplate.exchange(baseUrl, HttpMethod.DELETE, null, Order.class);
    	
    	Assert.assertEquals(response.getStatusCode(), HttpStatus.NO_CONTENT);
    }
 
}

