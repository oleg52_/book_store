package com.pracrice.bookstore.bookstore.controllerTest;

import java.net.URI;
import java.net.URISyntaxException;
import java.sql.Date;
import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.practice.bookstore.bookstore.BookStoreApplication;
import com.practice.bookstore.bookstore.entity.Book;
import com.practice.bookstore.bookstore.entity.Order_item;
import com.practice.bookstore.bookstore.repository.Order_itemRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT,classes = BookStoreApplication.class)
public class Order_itemControllerTest {
	@Autowired
    private TestRestTemplate restTemplate;
	
	@Autowired
	private Order_itemRepository repository;
	
	@LocalServerPort
    private int randomServerPort;
	
	@After
	public void resetDb() {
		repository.deleteAll();
	    repository.flush();
	}
	
	public Order_item createTestOrder_item(String name) {
		Book book = new Book(name,12,"by 1",new Date(System.currentTimeMillis()),"desc1", 122, null, null);
		Order_item order_item = new Order_item(book, 12, 144, null);
		return repository.saveAndFlush(order_item);
	}
	
    @Test
    public void testAddOrder_itemSuccess() throws URISyntaxException 
    {
        String baseUrl = "http://localhost:" + randomServerPort + "/api/order_items";
        URI uri = new URI(baseUrl);
         
        HttpHeaders headers = new HttpHeaders();
        headers.set("X-COM-PERSIST", "true");      
        headers.set("Content-Type", "application/json");    
        
        Book book = new Book("Ord_itmBook1",12,"by 1",new Date(System.currentTimeMillis()),"desc1", 122, null, null);
		Order_item order_item = new Order_item(book, 12, 144, null);        
		HttpEntity<Order_item> request = new HttpEntity<>(order_item, headers);
         
        ResponseEntity<Order_item> result = this.restTemplate.postForEntity(uri, request, Order_item.class);
         
        //Verify request succeed
        Assert.assertEquals(result.getStatusCodeValue(), 201);
        Assert.assertNotNull(result.getBody().getId());
        Assert.assertEquals(result.getBody().getQuantity(), 12);
        Assert.assertEquals(result.getBody().getPrice(), 144, 0);
        Assert.assertNull(result.getBody().getOrder());
        Assert.assertNotNull(result.getBody().getBook());
        Assert.assertEquals(result.getBody().getBook().getName(), "Ord_itmBook1");
    }
    
    @Test
    public void testGetOrder_itemSuccess() {
    	String baseUrl = "http://localhost:" + randomServerPort + "/api/order_items/{id}";
        
        long id = createTestOrder_item("Order_itemBook1").getId();
        Order_item result = restTemplate.getForObject(baseUrl, Order_item.class,id);
        
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getId());
        Assert.assertEquals(result.getQuantity(), 12);
        Assert.assertEquals(result.getPrice(), 144, 0);
        Assert.assertNull(result.getOrder());
        Assert.assertNotNull(result.getBook());
        Assert.assertEquals(result.getBook().getName(), "Order_itemBook1");
    }
    
    @Test
    public void testPutOrder_itemSuccess() {
    	String baseUrl = "http://localhost:" + randomServerPort + "/api/order_items/{id}";
    	
    	long id = createTestOrder_item("Order_itemBook1").getId();
    	Book book = new Book("Ord_itmBookBook2",12,"by 2",new Date(System.currentTimeMillis()),"desc2", 122, null, null);
		Order_item order_item = new Order_item(book, 13, 145, null);            	
		HttpEntity<Order_item> entity = new HttpEntity<Order_item>(order_item);
    	ResponseEntity<Order_item> response = restTemplate.exchange(baseUrl, HttpMethod.PUT, entity, Order_item.class, id);
    	
    	Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);
    	Assert.assertNotNull(response.getBody().getId());
    	Assert.assertEquals(response.getBody().getQuantity(), 13);
        Assert.assertEquals(response.getBody().getPrice(), 145, 0);
        Assert.assertNull(response.getBody().getOrder());
        Assert.assertNotNull(response.getBody().getBook());
        Assert.assertEquals(response.getBody().getBook().getName(), "Ord_itmBookBook2");
    }
    
    @Test
    public void testGetAllOrder_items() {
    	String baseUrl = "http://localhost:" + randomServerPort + "/api/order_items";
    	
    	createTestOrder_item("Order_item1Book1");
    	createTestOrder_item("Order_item2Book2");
    	ResponseEntity<List<Order_item>> response = restTemplate.exchange(baseUrl, HttpMethod.GET, null,
    	    new ParameterizedTypeReference<List<Order_item>>() {
    	    });
    	  List<Order_item> order_items = response.getBody();
    	  Assert.assertEquals(order_items.size(), 2);
    	  Assert.assertEquals(order_items.get(0).getQuantity(), 12);
    	  Assert.assertEquals(order_items.get(1).getQuantity(), 12);
    	  Assert.assertEquals(order_items.get(0).getBook().getName(), "Order_item1Book1");
          Assert.assertEquals(order_items.get(1).getBook().getName(), "Order_item2Book2");
    }
    
    @Test
    public void testDeleteOrder_itemSuccess() {
    	String baseUrl = "http://localhost:" + randomServerPort + "/api/order_items/{id}";
    	
    	long id = createTestOrder_item("Order_itemBook1").getId();
    	ResponseEntity<Order_item> response = restTemplate.exchange(baseUrl, HttpMethod.DELETE, null, Order_item.class, id);
    	
    	Assert.assertEquals(response.getStatusCode(), HttpStatus.NO_CONTENT);
    }
    
    @Test
    public void testDeleteAllAuthorsSuccess() {
    	String baseUrl = "http://localhost:" + randomServerPort + "/api/order_items";
    	
    	createTestOrder_item("Order_itemBook1");
    	createTestOrder_item("Order_itemBook2");
    	ResponseEntity<Order_item> response = restTemplate.exchange(baseUrl, HttpMethod.DELETE, null, Order_item.class);
    	
    	Assert.assertEquals(response.getStatusCode(), HttpStatus.NO_CONTENT);
    }
 
}
