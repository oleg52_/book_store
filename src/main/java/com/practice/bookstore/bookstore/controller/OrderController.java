package com.practice.bookstore.bookstore.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.practice.bookstore.bookstore.entity.Book;
import com.practice.bookstore.bookstore.entity.Order;
import com.practice.bookstore.bookstore.entity.Order_item;
import com.practice.bookstore.bookstore.repository.OrderRepository;


@RestController
@RequestMapping("/api")
public class OrderController {
	
	@Autowired
	OrderRepository orderRepository;
	
	@GetMapping("/orders")
	  public ResponseEntity<List<Order>> getAllCathegories() {
	    try {
	      List<Order> orders = new ArrayList<Order>();
	      orderRepository.findAll().forEach(orders::add);
	      if (orders.isEmpty()) {
	        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	      }

	      return new ResponseEntity<>(orders, HttpStatus.OK);
	    } catch (Exception e) {
	      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
	    }
	  }
	
	@GetMapping("/orders/{id}")
	  public ResponseEntity<Order> getOrderById(@PathVariable("id") long id) {
	    Optional<Order> orderData = orderRepository.findById(id);

	    if (orderData.isPresent()) {
	      return new ResponseEntity<>(orderData.get(), HttpStatus.OK);
	    } else {
	      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	    }
	  }
	
	  @PostMapping("/orders")
	  public ResponseEntity<Order> createOrder(@RequestBody Order order) {
	    try {
	      Order _order = orderRepository
	          .save(new Order(order.getName(), order.getCreated_by(), order.getCreation_date(),
	        		  order.getTotal_price(), order.getOrder_items()));
	      return new ResponseEntity<>(_order, HttpStatus.CREATED);
	    } catch (Exception e) {
	      return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED);
	    }
	  }

	  @PutMapping("/orders/{id}")
	  public ResponseEntity<Order> updateOrder(@PathVariable("id") long id, @RequestBody Order order) {
	    Optional<Order> orderData = orderRepository.findById(id);

	    if (orderData.isPresent()) {
	      Order _order = orderData.get();
	      _order.setName(order.getName());
	      _order.setCreated_by(order.getCreated_by());
	      _order.setCreation_date(order.getCreation_date());
	      _order.setTotal_price(order.getTotal_price());
	      for(Order_item order_item: order.getOrder_items())
	    	  _order.addOrder_item(order_item);
	      return new ResponseEntity<>(orderRepository.save(_order), HttpStatus.OK);
	    } else {
	      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	    }
	  }

	  @DeleteMapping("/orders/{id}")
	  public ResponseEntity<HttpStatus> deleteOrder(@PathVariable("id") long id) {
	    try {
	      orderRepository.deleteById(id);
	      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	    } catch (Exception e) {
	      return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
	    }
	  }

	  @DeleteMapping("/orders")
	  public ResponseEntity<HttpStatus> deleteAllOrders() {
	    try {
	      orderRepository.deleteAll();
	      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	    } catch (Exception e) {
	      return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
	    }

	  }

}
