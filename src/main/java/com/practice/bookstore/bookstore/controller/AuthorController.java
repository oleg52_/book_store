package com.practice.bookstore.bookstore.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.practice.bookstore.bookstore.entity.Author;
import com.practice.bookstore.bookstore.entity.Book;
import com.practice.bookstore.bookstore.repository.AuthorRepository;



@RestController
@RequestMapping("/api")
public class AuthorController {
	
	@Autowired
	AuthorRepository authorRepository;
	
	@GetMapping("/authors")
	  public ResponseEntity<List<Author>> getAllAuthors() {
	    try {
	      List<Author> authors = new ArrayList<Author>();
	      authorRepository.findAll().forEach(authors::add);
	      if (authors.isEmpty()) {
	        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	      }

	      return new ResponseEntity<>(authors, HttpStatus.OK);
	    } catch (Exception e) {
	      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
	    }
	  }
	
	@GetMapping("/authors/{id}")
	  public ResponseEntity<Author> getAuthorById(@PathVariable("id") long id) {
	    Optional<Author> authorData = authorRepository.findById(id);

	    if (authorData.isPresent()) {
	      return new ResponseEntity<>(authorData.get(), HttpStatus.OK);
	    } else {
	      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	    }
	  }
	
	@PostMapping("/authors") 
	  public ResponseEntity<Author> createAuthor(@RequestBody Author author) {
	    try {
	      Author _author = authorRepository
	          .save(new Author(author.getFirst_name(),author.getLast_name(), author.getBirth_date(),
	        		  author.getGender(), author.getWritten_books()));
	      return new ResponseEntity<>(_author, HttpStatus.CREATED);
	    } catch (Exception e) {
	      return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED);
	    }
	  }

	  @PutMapping("/authors/{id}")
	  public ResponseEntity<Author> updateAuthor(@PathVariable("id") long id, @RequestBody Author author) {
	    Optional<Author> authorData = authorRepository.findById(id);

	    if (authorData.isPresent()) {
	    	Author _author = authorData.get();
	    	_author.setFirst_name(author.getFirst_name());
	    	_author.setLast_name(author.getLast_name());
	    	_author.setBirth_date(author.getBirth_date());
	    	_author.setGender(author.getGender());
	    	if(author.getWritten_books() != null)
	    		for(Book book: author.getWritten_books())
	    			_author.addWritten_book(book);
	      return new ResponseEntity<>(authorRepository.save(_author), HttpStatus.OK);
	    } else {
	      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	    }
	  }

	  @DeleteMapping("/authors/{id}")
	  public ResponseEntity<HttpStatus> deleteAuthor(@PathVariable("id") long id) {
	    try {
	      authorRepository.deleteById(id);
	      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	    } catch (Exception e) {
	      return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
	    }
	  }

	  @DeleteMapping("/authors")
	  public ResponseEntity<HttpStatus> deleteAllAuthors() {
	    try {
	      authorRepository.deleteAll();
	      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	    } catch (Exception e) {
	      return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
	    }

	  }  

}