package com.practice.bookstore.bookstore.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.practice.bookstore.bookstore.entity.Book;
import com.practice.bookstore.bookstore.entity.Order_item;
import com.practice.bookstore.bookstore.repository.Order_itemRepository;


@RestController
@RequestMapping("/api")
public class Order_itemController {
	
	@Autowired
	Order_itemRepository cathegoryRepository;
	
	@GetMapping("/order_items")
	  public ResponseEntity<List<Order_item>> getAllOrder_items() {
	    try {
	      List<Order_item> order_items = new ArrayList<Order_item>();
	      cathegoryRepository.findAll().forEach(order_items::add);
	      if (order_items.isEmpty()) {
	        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	      }

	      return new ResponseEntity<>(order_items, HttpStatus.OK);
	    } catch (Exception e) {
	      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
	    }
	  }
	
	@GetMapping("/order_items/{id}")
	  public ResponseEntity<Order_item> getOrder_itemById(@PathVariable("id") long id) {
	    Optional<Order_item> cathegoryData = cathegoryRepository.findById(id);

	    if (cathegoryData.isPresent()) {
	      return new ResponseEntity<>(cathegoryData.get(), HttpStatus.OK);
	    } else {
	      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	    }
	  }
	
	  @PostMapping("/order_items")
	  public ResponseEntity<Order_item> createOrder_item(@RequestBody Order_item order_item) {
	    try {
	      Order_item _order_item = cathegoryRepository
	          .save(new Order_item(order_item.getBook(), order_item.getQuantity(), order_item.getPrice(), order_item.getOrder()));
	      return new ResponseEntity<>(_order_item, HttpStatus.CREATED);
	    } catch (Exception e) {
	      return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED);
	    }
	  }

	  @PutMapping("/order_items/{id}")
	  public ResponseEntity<Order_item> updateOrder_item(@PathVariable("id") long id, @RequestBody Order_item order_item) {
	    Optional<Order_item> order_itemData = cathegoryRepository.findById(id);

	    if (order_itemData.isPresent()) {
	      Order_item _order_item = order_itemData.get();
	      _order_item.setBook(order_item.getBook());
	      _order_item.setPrice(order_item.getPrice());
	      _order_item.setQuantity(order_item.getQuantity());
	      return new ResponseEntity<>(cathegoryRepository.save(_order_item), HttpStatus.OK);
	    } else {
	      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	    }
	  }

	  @DeleteMapping("/order_items/{id}")
	  public ResponseEntity<HttpStatus> deleteOrder_item(@PathVariable("id") long id) {
	    try {
	      cathegoryRepository.deleteById(id);
	      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	    } catch (Exception e) {
	      return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
	    }
	  }

	  @DeleteMapping("/order_items")
	  public ResponseEntity<HttpStatus> deleteAllOrder_items() {
	    try {
	      cathegoryRepository.deleteAll();
	      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	    } catch (Exception e) {
	      return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
	    }

	  }

}
