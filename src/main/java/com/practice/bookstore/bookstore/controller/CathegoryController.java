package com.practice.bookstore.bookstore.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.practice.bookstore.bookstore.entity.Book;
import com.practice.bookstore.bookstore.entity.Cathegory;
import com.practice.bookstore.bookstore.repository.CathegoryRepository;


@RestController
@RequestMapping("/api")
public class CathegoryController {
	
	@Autowired
	CathegoryRepository cathegoryRepository;
	
	@GetMapping("/cathegories")
	  public ResponseEntity<List<Cathegory>> getAllCathegories() {
	    try {
	      List<Cathegory> cathegories = new ArrayList<Cathegory>();
	      cathegoryRepository.findAll().forEach(cathegories::add);
	      if (cathegories.isEmpty()) {
	        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	      }

	      return new ResponseEntity<>(cathegories, HttpStatus.OK);
	    } catch (Exception e) {
	      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
	    }
	  }
	
	@GetMapping("/cathegories/{id}")
	  public ResponseEntity<Cathegory> getCathegoryById(@PathVariable("id") long id) {
	    Optional<Cathegory> cathegoryData = cathegoryRepository.findById(id);

	    if (cathegoryData.isPresent()) {
	      return new ResponseEntity<>(cathegoryData.get(), HttpStatus.OK);
	    } else {
	      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	    }
	  }
	
	  @PostMapping("/cathegories")
	  public ResponseEntity<Cathegory> createCathegory(@RequestBody Cathegory cathegory) {
	    try {
	      Cathegory _cathegory = cathegoryRepository
	          .save(new Cathegory(cathegory.getName(), cathegory.getCreated_by(), 
	        		  cathegory.getCreation_date(), cathegory.getContained_books()));
	      return new ResponseEntity<>(_cathegory, HttpStatus.CREATED);
	    } catch (Exception e) {
	      return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED);
	    }
	  }

	  @PutMapping("/cathegories/{id}")
	  public ResponseEntity<Cathegory> updateCathegory(@PathVariable("id") long id, @RequestBody Cathegory cathegory) {
	    Optional<Cathegory> cathegoryData = cathegoryRepository.findById(id);

	    if (cathegoryData.isPresent()) {
	      Cathegory _cathegory = cathegoryData.get();
	      _cathegory.setName(cathegory.getName());
	      _cathegory.setCreated_by(cathegory.getCreated_by());
	      _cathegory.setCreation_date(cathegory.getCreation_date());
	      for(Book book: cathegory.getContained_books())
	    	  _cathegory.addContained_book(book);
	      return new ResponseEntity<>(cathegoryRepository.save(_cathegory), HttpStatus.OK);
	    } else {
	      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	    }
	  }

	  @DeleteMapping("/cathegories/{id}")
	  public ResponseEntity<HttpStatus> deleteCathegory(@PathVariable("id") long id) {
	    try {
	      cathegoryRepository.deleteById(id);
	      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	    } catch (Exception e) {
	      return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
	    }
	  }

	  @DeleteMapping("/cathegories")
	  public ResponseEntity<HttpStatus> deleteAllCathegorys() {
	    try {
	      cathegoryRepository.deleteAll();
	      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	    } catch (Exception e) {
	      return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
	    }

	  }

}
