package com.practice.bookstore.bookstore.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import com.practice.bookstore.bookstore.entity.Author;

public interface AuthorRepository extends JpaRepository<Author, Long> {
	
}
