package com.practice.bookstore.bookstore.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.practice.bookstore.bookstore.entity.Order_item;

public interface Order_itemRepository extends JpaRepository<Order_item, Long> {

}