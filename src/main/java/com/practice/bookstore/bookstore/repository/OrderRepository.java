package com.practice.bookstore.bookstore.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.practice.bookstore.bookstore.entity.Order;

public interface OrderRepository extends JpaRepository<Order, Long> {

}