package com.practice.bookstore.bookstore.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.practice.bookstore.bookstore.entity.Cathegory;

public interface CathegoryRepository extends JpaRepository<Cathegory, Long> {

}
