package com.practice.bookstore.bookstore.entity;

import java.sql.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "cathegory")
public class Cathegory {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column(name = "name")
	private String name;

	@Column(name = "created_by")
	private String created_by;
	
	@Column(name = "creation_date")
	private Date creation_date;
	
	@JsonManagedReference(value = "contained_books")
	@OneToMany(
	        mappedBy = "cathegory",
	        cascade = CascadeType.ALL,
	        orphanRemoval = true
	    )
	private List<Book> contained_books;

	public Cathegory() {
		
	}

	public Cathegory(String name, String created_by, Date creation_date, List<Book> contained_books) {
		super();
		this.name = name;
		this.created_by = created_by;
		this.creation_date = creation_date;
		this.contained_books = contained_books;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCreated_by() {
		return created_by;
	}

	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}

	public Date getCreation_date() {
		return creation_date;
	}

	public void setCreation_date(Date creation_date) {
		this.creation_date = creation_date;
	}

	public List<Book> getContained_books() {
		return contained_books;
	}

	public void setContained_books(List<Book> contained_books) {
		this.contained_books = contained_books;
	}
	
	public void addContained_book(Book book) {
		contained_books.add(book);
		book.setCathegory(this);
	}
 
    public void removeContained_book(Book book) {
    	contained_books.remove(book);
        book.setCathegory(null);
    }

}

