package com.practice.bookstore.bookstore.entity;

import java.sql.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "book")
public class Book {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column(name = "name")
	private String name;

	@Column(name = "sku")
	private int sku;

	@Column(name = "created_by")
	private String created_by;
	
	@Column(name = "creation_date")
	private Date creation_date;
	
	@Column(name = "description")
	private String description;
	
	@Column(name = "price")
	private double price;
	
	@JsonBackReference(value = "written_books")
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Author author;
	
	@JsonBackReference(value = "contained_books")
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Cathegory cathegory;

	public Book() {

	}

	public Book(String name, int sku, String created_by, Date creation_date, String description, double price, 
			Author author, Cathegory cathegory) {
		super();
		this.name = name;
		this.sku = sku;
		this.created_by = created_by;
		this.creation_date = creation_date;
		this.description = description;
		this.price = price;
		this.author = author;
		this.cathegory = cathegory;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getSku() {
		return sku;
	}

	public void setSku(int sku) {
		this.sku = sku;
	}

	public String getCreated_by() {
		return created_by;
	}

	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}

	public Date getCreation_date() {
		return creation_date;
	}

	public void setCreation_date(Date creation_date) {
		this.creation_date = creation_date;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}
	
	public Cathegory getCathegory() {
		return cathegory;
	}

	public void setCathegory(Cathegory cathegory) {
		this.cathegory = cathegory;
	}

	
}
