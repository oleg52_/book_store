package com.practice.bookstore.bookstore.entity;

import java.sql.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "orders")
public class Order {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column(name = "name")
	private String name;

	@Column(name = "created_by")
	private String created_by;
	
	@Column(name = "creation_date")
	private Date creation_date;
	
	@Column(name = "total_price")
	private double total_price;
	
	@JsonManagedReference(value = "order_id")
	@OneToMany(
	        mappedBy = "order",
	        cascade = CascadeType.ALL,
	        orphanRemoval = true
	    )
	private List<Order_item> order_items;
	
	public Order() {
		
	}

	public Order(String name, String created_by, Date creation_date, double total_price,
			List<Order_item> order_items) {
		super();
		this.name = name;
		this.created_by = created_by;
		this.creation_date = creation_date;
		this.total_price = total_price;
		this.order_items = order_items;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCreated_by() {
		return created_by;
	}

	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}

	public Date getCreation_date() {
		return creation_date;
	}

	public void setCreation_date(Date creation_date) {
		this.creation_date = creation_date;
	}

	public double getTotal_price() {
		return total_price;
	}

	public void setTotal_price(double total_price) {
		this.total_price = total_price;
	}

	public List<Order_item> getOrder_items() {
		return order_items;
	}

	public void setOrder_items(List<Order_item> order_items) {
		if (this.order_items == null) {
            this.order_items = order_items;
        } else if(this.order_items != order_items){ // not the same instance, in other case we can get ConcurrentModificationException from hibernate AbstractPersistentCollection
            this.order_items.clear();
            if(order_items != null){
            	this.order_items.addAll(order_items);
            }
        }
	}
	
	public void addOrder_item(Order_item order_item) {
		order_items.add(order_item);
		order_item.setOrder(this);
	}
 
    public void removeOrder_item(Order_item order_item) {
    	order_items.remove(order_item);
    	order_item.setOrder(null);
    }


}
