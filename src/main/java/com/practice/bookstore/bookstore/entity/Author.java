package com.practice.bookstore.bookstore.entity;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "author")
public class Author {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column(name = "firstName")
	private String firstName;

	@Column(name = "last_name")
	private String last_name;
	
	@Column(name = "birth_date")
	private Date birth_date;
	
	@Column(name = "gender")
	private String gender;
	
	@JsonManagedReference(value = "written_books")
	@OneToMany(
	        mappedBy = "author",
	        cascade = CascadeType.ALL,
	        orphanRemoval = true
	    )
	private List<Book> written_books;
	
	public Author() {
		
	}

	public Author(String first_name, String last_name, Date birth_date, String gender,
			List<Book> written_books) {
		super();
		this.firstName = first_name;
		this.last_name = last_name;
		this.birth_date = birth_date;
		this.gender = gender;
		this.written_books = written_books;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFirst_name() {
		return firstName;
	}

	public void setFirst_name(String first_name) {
		this.firstName = first_name;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public Date getBirth_date() {
		return birth_date;
	}

	public void setBirth_date(Date birth_date) {
		this.birth_date = birth_date;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public List<Book> getWritten_books() {
		return written_books;
	}

	public void setWritten_books(List<Book> written_books) {
	    if (this.written_books == null) {
            this.written_books = written_books;
        } else if(this.written_books != written_books){ // not the same instance, in other case we can get ConcurrentModificationException from hibernate AbstractPersistentCollection
            this.written_books.clear();
            if(written_books != null){
            	this.written_books.addAll(written_books);
            }
        }
	}
	
	public void addWritten_book(Book book) {
        written_books.add(book);
        book.setAuthor(this);
    }
 
    public void removeWritten_book(Book book) {
    	written_books.remove(book);
        book.setAuthor(null);
    }

	

}
